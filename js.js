`use strict`;

function firstProg() {

  do {
    let firstVal = parseInt(prompt(`I want a number!`));
    let secondVal = parseInt(prompt(`Give me one more!`));

    if (isNaN(firstVal) || isNaN(secondVal)) {
      alert(`I want THE NUMBERS! Try again.`);
      continue;
    } else {
      compare = firstVal > secondVal ? `>` : firstVal < secondVal ? `<` : `=`;
      alert(`${firstVal} ${compare} ${secondVal}`);
      return;
    }
  } while (1)

}

function secondProg() {
  let startYear = parseInt(prompt(`Enter first year`));
  let endYear = parseInt(prompt(`Enter the last year`));

  if (isNaN(startYear) || isNaN(endYear)) {
    alert(`I want THE NUMBERS! Try again.`);
    return;
  } else {
    if (startYear > endYear) {
      [startYear, endYear] = [endYear, startYear];
      // startYear = startYear ^ endYear;
      // endYear = startYear ^ endYear;
      // startYear = startYear ^ endYear;
    }
    let year = startYear + startYear%4;
    let years =[];
    for (;year<=endYear;year+=4) {
      if(year%400==0) continue;
      years.push(year);
    }
    alert(`These years between ${startYear}-${endYear} is leap: ${years.toString()}`)
  }
}

function thirdProg() {
  let sum, someVal;
  while ((someVal = prompt(`Enter the number:`)) !== null) {
    someVal = parseInt(someVal);
    if (!isNaN(someVal)) {
      if (sum !== undefined) sum += someVal;
      else sum = someVal;
    }
  }
  if (isNaN(sum)) alert(`I no have any numbers.`);
  else alert(`Sum of numbers is: ${sum}`);
}
